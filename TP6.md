# TP6 Linux

## Partie 1 : Préparation de la machine backup.tp6.linux

### I : Ajout de disque

- Ajouter un disque dur de 5Go à la VM ```backup.tp6.linux```

```bash
# on utilise grep pour trouver le disque dur de 5Giga
[scooby@backup ~]$  lsblk | grep 5G
sdb           8:16   0    5G  0 disk
```

### II : Partitioning

- Partitionner le disque à l'aide de LVM

```bash
[scooby@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for scooby:
  Physical volume "/dev/sdb" successfully created.

[scooby@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g

[scooby@backup ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               y7pJpY-oEqj-srF6-NG9F-mDMY-o6nY-IIF7kP

  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               Ue5qlX-te9L-AhGq-R3Lj-ywo7-KiHz-C7Jhef  
```

```bash
[scooby@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created

[scooby@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created

[scooby@backup ~]$ sudo vgs
[sudo] password for scooby:
  VG     #PV #LV #SN Attr   VSize  VFree
  backup   1   0   0 wz--n- <5.00g <5.00g
  rl       1   2   0 wz--n- <7.00g     0

[scooby@backup ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               backup
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <5.00 GiB
  PE Size               4.00 MiB
  Total PE              1279
  Alloc PE / Size       0 / 0
  Free  PE / Size       1279 / <5.00 GiB
  VG UUID               d06jua-5CoC-lSTE-aPPX-PfdN-dIAa-oKqnC5

[...]
```

```bash
[scooby@backup ~]$ sudo lvcreate -l 100%FREE backup -n last_data
[sudo] password for scooby:
  Logical volume "last_data" created.

[scooby@backup ~]$ sudo lvs
  LV        VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  last_data backup -wi-a-----  <5.00g
  root      rl     -wi-ao----  <6.20g
  swap      rl     -wi-ao---- 820.00m

[scooby@backup ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/backup/last_data
  LV Name                last_data
  VG Name                backup
  LV UUID                yhuUW4-NQNo-jL83-RxQi-hytD-nHzD-awVR1e
  LV Write Access        read/write
  LV Creation host, time backup.tp6.linux, 2021-12-03 16:37:23 +0100
  LV Status              available
  # open                 0
  LV Size                <5.00 GiB
  Current LE             1279
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

[...]
```

- Formater la partition 

```bash
[scooby@backup ~]$ sudo mkfs -t ext4 /dev/backup/last_data
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: f2190fd1-782a-4b2c-b91c-2ae6e305487b
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

- Monter la partition

```bash
[scooby@backup /]$ sudo mount /dev/backup/last_data /backup

[scooby@backup /]$ df -h | grep backup
/dev/mapper/backup-last_data  4.9G   20M  4.6G   1% /backup
```

```bash
[scooby@backup ~]$ sudo nano /etc/fstab
[sudo] password for scooby:

[scooby@backup ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Tue Nov 23 14:19:30 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=8b3feda6-3fb4-4087-9e4a-e35644fee3ec /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/backup/last_data   /backup                 ext4    defaults        0 0
```

```bash
[scooby@backup ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /backup does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/backup                  : successfully mounted

[scooby@backup ~]$ sudo restorecon -R /backup

[scooby@backup ~]$ sudo umount /backup

[scooby@backup ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
/backup                  : successfully mounted
```
## Partie 2 : Setup du serveur NFS sur backup.tp6.linux

- Préparer les dossiers à partager

```bash
[scooby@backup backup]$ sudo mkdir web.tp6.linux
[sudo] password for scooby:

[scooby@backup backup]$ ls
lost+found  web.tp6.linux
```

```bash
[scooby@backup backup]$ sudo mkdir db.tp6.linux

[scooby@backup backup]$ ls
db.tp6.linux  lost+found  web.tp6.linux
```

- Install du serveur NFS 

```bash
[scooby@backup ~]$ sudo dnf install nfs-utils
Rocky Linux 8 - AppStream                                               5.6 kB/s | 4.8 kB     00:00
Rocky Linux 8 - AppStream                                               462 kB/s | 8.3 MB     00:18
Rocky Linux 8 - BaseOS                                                  6.9 kB/s | 4.3 kB     00:00
Rocky Linux 8 - BaseOS                                                  283 kB/s | 3.5 MB     00:12
Rocky Linux 8 - Extras                                                  4.0 kB/s | 3.5 kB     00:00
Rocky Linux 8 - Extras                                                   12 kB/s |  10 kB     00:00
Dependencies resolved.
[...]

Installed:
  gssproxy-0.8.0-19.el8.x86_64     keyutils-1.5.10-9.el8.x86_64  libverto-libevent-0.3.0-5.el8.x86_64
  nfs-utils-1:2.3.3-46.el8.x86_64  rpcbind-1.2.5-8.el8.x86_64

Complete!
```

- Conf du serveur NFS

```bash
[scooby@backup ~]$ sudo nano /etc/idmapd.conf

[scooby@backup ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux
[...]
```

```bash
[scooby@backup ~]$ sudo nano /etc/exports

[scooby@backup ~]$ sudo cat /etc/exports
/backup/web.tp6.linux/  10.5.1.11/24(rw,no_root_squash)
/backup/db.tp6.linux/   10.5.1.12/24(rw,no_root_squash)
```

- Démarrer le service

```bash
[scooby@backup ~]$ sudo systemctl start nfs-server
[sudo] password for scooby:

[scooby@backup ~]$ systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Sun 2021-12-05 13:22:22 CET; 9s ago
  Process: 4910 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy>
  Process: 4898 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 4897 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 4910 (code=exited, status=0/SUCCESS)

Dec 05 13:22:22 backup.tp6.linux systemd[1]: Starting NFS server and services...
Dec 05 13:22:22 backup.tp6.linux systemd[1]: Started NFS server and services.

[scooby@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

- Firewall

```bash
[scooby@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
[sudo] password for scooby:
success

[scooby@backup ~]$ sudo ss -lptn | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```
## Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux

- Installation

```bash
[scooby@web ~]$ sudo dnf install nfs-utils
[sudo] password for scooby:
Rocky Linux 8 - AppStream                                               3.5 kB/s | 4.8 kB     00:01
Rocky Linux 8 - AppStream                                                46 kB/s | 8.3 MB     03:03
Rocky Linux 8 - BaseOS                                               [...]
Installed:
  gssproxy-0.8.0-19.el8.x86_64     keyutils-1.5.10-9.el8.x86_64  libverto-libevent-0.3.0-5.el8.x86_64
  nfs-utils-1:2.3.3-46.el8.x86_64  rpcbind-1.2.5-8.el8.x86_64

Complete!
```

- Configuration

```bash
[scooby@web ~]$ cd /srv

[scooby@web srv]$ sudo mkdir backup
[sudo] password for scooby:
```

```bash
[scooby@web ~]$ sudo nano /etc/idmapd.conf

[scooby@web ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux
[...]
```

- Montage

```bash
[scooby@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux/ /srv/backup

[scooby@web ~]$ df -h | grep back
10.5.1.13:/backup/web.tp6.linux  4.9G   20M  4.6G   1% /srv/backup

[scooby@web ~]$ ls -l
total 0
drwxr-xr-x. 3 roxanne root 45 Dec  7 10:16 backup
drwxr-xr-x. 2 roxanne root 27 Nov 28 21:36 bin
```

```bash
[scooby@web ~]$ sudo nano /etc/fstab

[scooby@web ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Tue Nov 23 14:19:30 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=8b3feda6-3fb4-4087-9e4a-e35644fee3ec /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
10.5.1.13:/backup/web.tp6.linux/        /srv/backup             nfs     defaults        0 0
```

```bash
[scooby@web ~]$ sudo umount 10.5.1.13:/backup/web.tp6.linux/

[scooby@web ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
/srv/backup              : successfully mounted
```

- Répeter les opérations sur ```db.tp6.linux```

```bash
[scooby@dp ~]$ sudo dnf install nfs-utils
Rocky Linux 8 - AppStream                                                                                                                                                          6.0 kB/s | 4.8 kB     00:00
Rocky Linux 8 - AppStream                                                                                                                                                          447 kB/s | 8.3 MB     00:18
Rocky Linux 8 - BaseOS                                                                                                                                                             5.1 kB/s | 4.3 kB     00:00
Rocky Linux 8 - BaseOS                                                                                                                                                             471 kB/s | 3.5 MB     00:07
Rocky Linux 8 - Extras                                                                                                                                                             6.3 kB/s | 3.5 kB     00:00
Rocky Linux 8 - Extras                                                                                                                                                              11 kB/s |  10 kB     00:00
Dependencies resolved.
[...]
Complete!
```

```bash
[scooby@dp srv]$ sudo mkdir backup

[scooby@dp srv]$ ls
backup

[scooby@dp ~]$ sudo nano /etc/idmapd.conf

[scooby@dp ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux
[...]
```

```bash
[scooby@dp ~]$ df -h | grep back
10.5.1.13:/backup/db.tp6.linux  4.9G   20M  4.6G   1% /srv/backup

[scooby@dp ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignoredcd 
/srv/backup              : successfully mounted
```

## Partie 4 : Scripts de sauvegarde

### I : Sauvegarde web

- Ecrire un script qui sauvegarde les données de NextCloud

```bash
Je n'arrive pas à faire des scripts Bash
```
