# TP 5 

## I - Setup up DB

### 1- Install MariaDB

**Installer MariaDB sur la machine** ```db.tp5.linux```
```bash=
[scooby@db ~]$ sudo dnf install mariadb-server
[sudo] password for scooby: 
Last metadata expiration check: 21:18:16 ago on Thu 25 Nov 2021 04:56:33 PM CET.
Dependencies resolved.
[...]
```

**Le service MariaDB**
```bash=
[scooby@db ~]$ sudo systemctl start mariadb.service

[scooby@db ~]$ systemctl enable mariadb
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: Scooby (scooby)
Password: 
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: Scooby (scooby)
Password: 
==== AUTHENTICATION COMPLETE ====

[scooby@db ~]$ systemctl status mariadb.service 
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-11-26 14:26:29 CET; 1min 19s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 26011 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4935)
   Memory: 84.2M
   CGroup: /system.slice/mariadb.service
           └─26011 /usr/libexec/mysqld --basedir=/usr

Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: See the MariaDB Knowledgebase at http://mariadb.com/kb or>
Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: MySQL manual for more instructions.
Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: Please report any problems at http://mariadb.org/jira
Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: The latest information about MariaDB is available at http>
Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: You can find additional information about the MySQL part >
Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: http://dev.mysql.com
Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: Consider joining MariaDB's strong and vibrant community:
Nov 26 14:26:29 db.tp5.linux mysql-prepare-db-dir[25908]: https://mariadb.org/get-involved/
Nov 26 14:26:29 db.tp5.linux mysqld[26011]: 2021-11-26 14:26:29 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-MariaD>
Nov 26 14:26:29 db.tp5.linux systemd[1]: Started MariaDB 10.3 database server.

[scooby@db ~]$ sudo ss -lntp | grep mysql
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=26011,fd=21))

[scooby@db ~]$ ps aux | grep MariaDB
scooby     26240  0.0  0.1 221928  1140 pts/0    S+   14:34   0:00 grep --color=auto MariaDB
```

**Firewall**

```bash=
[scooby@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success

[scooby@db ~]$ sudo firewall-cmd --reload
success

[scooby@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

### 2- Conf Maria

**Remove anonymous users?** - > Cette question permet de savoir si on accepte que les personnes non enregistées se connectent à la Base de données
Répondre yes = Il faut donc ce login pour pouvoir se connecter à la DB

**Disallow root login remotely?** - > Cette question permet de savoir si on veut que l'on puisse se connecter en root à distance
Répondre yes = Il faut donc être directement sur la machine pour pouvoir se connecter en root

**Remove test database and access to it?** - > Cette question permet de savoir si on veut supprimer la DB test et ses accès
Répondre yes = Cela permet d'éviter l'accès a la DB test et sa suppression

**Reload privilege tables now?** - > Cette question permet de savoir si on veut que les privilèges modifiés soient pris en compte immédiatement
Répondre yes = Cela permet de prendre en compte toutes les modifications immédiatement

### 3- Test

**Installez sur la machine** ```web.tp5.linux``` **la commande** ```mysql```

```bash=
[scooby@web ~]$ sudo dnf provides mysql
Last metadata expiration check: 0:07:14 ago on Fri 26 Nov 2021 05:33:22 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[scooby@web ~]$ sudo dnf install mysql
[sudo] password for scooby: 
Last metadata expiration check: 0:12:25 ago on Fri 26 Nov 2021 05:33:22 PM CET.
Dependencies resolved.
```
**Connexion a DB**

```bash= 
[scooby@web ~]$ mysql -u nextcloud -p -h 10.5.1.12 -P 3306 -D nextcloud
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.002 sec)
```

## II - Set up Web

### 1. Install Apache

#### A. Apache

**Installation Apache**

```bash=
[scooby@web ~]$ sudo dnf install httpd
[sudo] password for scooby: 
Last metadata expiration check: 0:06:50 ago on Mon 29 Nov 2021 04:01:04 PM CET.
Dependencies resolved.

[...]

Complete!
```

**Analyse du service Apache**

```bash=
[scooby@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: Scooby (scooby)
Password: 
==== AUTHENTICATION COMPLETE ====

[scooby@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[scooby@web ~]$ ps -aux | grep httpd
root        5334  0.1  1.3 280200 11364 ?        Ss   16:08   0:00 /usr/sbin/httpd -DFOREGROUND
apache      5335  0.0  1.0 294080  8452 ?        S    16:08   0:00 /usr/sbin/httpd -DFOREGROUND
apache      5336  0.0  1.7 1482996 14240 ?       Sl   16:08   0:00 /usr/sbin/httpd -DFOREGROUND
apache      5337  0.0  1.4 1351868 12192 ?       Sl   16:08   0:00 /usr/sbin/httpd -DFOREGROUND
apache      5338  0.0  1.4 1351868 12192 ?       Sl   16:08   0:00 /usr/sbin/httpd -DFOREGROUND
scooby      5580  0.0  0.1 221928  1160 pts/0    R+   16:09   0:00 grep --color=auto httpd

[scooby@web ~]$ sudo ss -ltunp | grep httpd
tcp   LISTEN 0      128                *:80              *:*    users:(("httpd",pid=5338,fd=4),("httpd",pid=5337,fd=4),("httpd",pid=5336,fd=4),("httpd",pid=5334,fd=4))
```

**Premier test** 

```bash= 
[scooby@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success

[scooby@web ~]$ sudo firewall-cmd --reload
success

[scooby@web ~]$ curl -I 10.5.1.11
HTTP/1.1 403 Forbidden
Date: Mon, 29 Nov 2021 15:37:51 GMT
Server: Apache/2.4.37 (rocky)
Last-Modified: Fri, 11 Jun 2021 15:23:57 GMT
ETag: "1dc5-5c47f18a65d40"
Accept-Ranges: bytes
Content-Length: 7621
Content-Type: text/html; charset=UTF-8
```

#### B. PHP

**Installation de PHP**

```bash
[scooby@web ~]$ sudo dnf install epel-release

[scooby@web ~]$ sudo dnf update

[scooby@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm [sudo] password for scooby:  Last metadata expiration check: 0:09:45 ago on Mon 29 Nov 2021 04:40:39 PM CET. remi-release-8.rpm                                                                                                                                                                                          158 kB/s |  26 kB     00:00     Dependencies resolved.
[...]
Complete!

[scooby@web ~]$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64                                                                                                                                                   2.1 kB/s | 858  B     00:00    
Remi's Modular repository for Enterprise Linux 8 - x86_64                                                                                                                                                   3.0 MB/s | 3.1 kB     00:00    
Importing GPG key 0x5F11735A:
[...]
Complete!

[scooby@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:00:29 ago on Mon 29 Nov 2021 04:51:05 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
[...]
Complete!
```

### 2. Conf Apache

**Analyser la conf Apache**

```bash=
[scooby@web conf]$ tail -n 1 httpd.conf 
IncludeOptional conf.d/*.conf
```

**Créer un VirtualHost qui accueillera NextCloud**

```bash
[scooby@web conf.d]$ cat nextcloud.conf 
<VirtualHost *:80>
 
  DocumentRoot /var/www/nextcloud/html/  

  ServerName  web.tp5.linux  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

**Configurer la racine web**

```bash=
[scooby@web www]$ sudo mkdir nextcloud nextcloud/html

[scooby@web www]$ cd nextcloud/html/

[scooby@web html]$ sudo chown -R apache:apache ../../nextcloud/
```

**Configurer PHP**

```bash
[scooby@web nextcloud]$ cat /etc/opt/remi/php74/php.ini | grep date.timezone
; http://php.net/date.timezone
date.timezone = "Europe/Paris"
```

### 3. Install NextCloud

**Récupérer NextCloud**

```bash=
[scooby@web nextcloud]$ cd

[scooby@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  9874k      0  0:00:15  0:00:15 --:--:-- 7791k

[scooby@web ~]$ ls
nextcloud-21.0.1.zip
```

**Ranger la chambre**

```bash=
[scooby@web ~]$ unzip nextcloud-21.0.1.zip

[scooby@web ~]$ sudo mv nextcloud/*  /var/www/nextcloud/html/

[scooby@web ~]$ ls /var/www/nextcloud/html/
3rdparty  apps  AUTHORS  config  console.php  COPYING  core  cron.php  index.html  index.php  lib  occ  ocm-provider  ocs  ocs-provider  public.php  remote.php  resources  robots.txt  status.php  themes  updater  version.php

[scooby@web ~]$ rm -rfd  nextcloud
```

### 4. Test

```bash=
 ~ > sudo nano /etc/hosts

 ~ > cat /etc/hosts | grep tp5
 10.5.1.11 web.tp5.linux

 ~ > curl -I 10.5.1.11
HTTP/1.1 200 OK
Date: Mon, 29 Nov 2021 16:44:08 GMT
Server: Apache/2.4.37 (rocky)
Last-Modified: Thu, 08 Apr 2021 13:31:16 GMT
ETag: "9c-5bf760fd1b100"
Accept-Ranges: bytes
Content-Length: 156
Content-Type: text/html; charset=UTF-8
```
