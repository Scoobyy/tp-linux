# TP Linux 1

## J'ai trouvé 6 façon de détruire une Machine Linux :

- Lorsque le terminal se lance, les clicks sont supprimés
- Lorsque le terminal se lance, La souris est inutilisable et le clavier est bloqué
- Lorsque le terminal se lance, les touches du clavier sont changées
- Lorsque le terminal se lance, la machine reboot
- Lorsque le terminal se lance, une image s'affiche que l'on ne peut pas enlever et le terminal est inutilisable
- Une fois l'ordinateur rallumer, On reste bloqué à la page de connexion

## 1 - Les clicks supprimés

```markdown
Lsmod
Sudo rmmod psmouse 
```
## 2 - Clavier bloqué et souris inutilisable

```markdown
Sudo apt-get install xtrlock
nano /home/gaby/Documents/.lock.sh
```
**Dans .lock.sh :**
``` markdown
#!/bin/sh
Xtrlock
```
**Dans le Terminale :**
```markdown
sudo -i
chmod -R 777 /home/gaby/Documents/.lock.sh
nano /home/gaby/.bashrc
```
**Tout à la fin de bashrc il faut écrire :**

```markdown
/home/gaby/Documents/.lock.sh
```
## 3 - Les touches du clavier changées

```markdown
sudo apt-get install setxkbmap
nano /home/gaby/Documents/.cheh.sh
```
**Dans .lock.sh :**
```markdown
#!/bin/sh
setxkbmap
```
**Dans le Terminal :**
```markdown
sudo -i
chmod -R 777 /home/gaby/Documents/.cheh.sh
nano /home/gaby/.bashrc
```

**Tout à la fin de bashrc il faut écrire :** 

```markdown
/home/gaby/Documents/.cheh.sh
```
## 4 - Le Terminal qui fait reboot
```markdown
nano /home/gaby/Documents/.rebout.sh
```

**Dans .rebout.sh :**

```mardown
#!/bin/sh
reboot
```
**Dans le Terminal :**
```mardown
sudo -i
chmod -R 777 /home/gaby/Documents/.rebout.sh
nano /home/gaby/.bashrc
```

**Tout à la fin de bashrc il faut écrire :**

```markdown
/home/gaby/Documents/.rebout.sh
```

## 5 - Image insupprimable et Terminal inutilisable

```markdown
Sudo apt-get install eog
```

**Puis il faut télécharger une image « pour moi un bisou »**

```markdown
nano /home/gaby/Documents/.bisous.sh
```

**Dans .bisous.sh :**

```markdown
While true
do
eog /home/gaby/Download/bise.jpg 
done
```

**Dans le Terminal :**

```markdown
sudo -i
chmod -R 777 /home/gaby/Documents/.bisous.sh
nano /home/gaby/.bashrc
```

**Tout à la fin de bashrc il faut écrire :**

```markdown
/home/gaby/Documents/.bisous.sh
```

## 6 - Page de Connexion bloqué

```markdown
Which bash
sudo rm /usr/bin/bash
```
