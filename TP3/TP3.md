# TP3 Linux


## Script carte d'identité

Voici le [script](https://gitlab.com/Scoobyy/tp-linux/-/blob/main/srv/idcard.sh) de l'Idcard

### Exemple d'exécution d'une sortie : 

```shell=
Machine name: laroumanie-aspirea51556
OS: Manjaro Linux and the kernel version is 5.13.19-2-MANJARO
IP: 127.0.1.1
RAM: 14Gi RAM restante sur 19Gi RAM totale
Disque : 59G space left
Top 5 processes by RAM usage :
/opt/discord/Discord
/usr/lib/opera/opera
/usr/bin/plasmashell
/usr/lib/opera/opera 

Listening ports: 
1716 : kdeconnec
6463 : Discord

Here's your random cat : https://cdn2.thecatapi.com/images/9f9.jpg
```
## Script Youtube-dl

Voici le [Script](https://gitlab.com/Scoobyy/tp-linux/-/blob/main/srv/yt.sh) du Youtube-dl

Voici les [Logs](https://gitlab.com/Scoobyy/tp-linux/-/blob/main/srv/download.log) des téléchargements avec le script

```bash=
~/Documents/Code/srv/yt > bash yt.sh

Video https://www.youtube.com/watch?v=z_HWtzUHm6s was downloaded.
File path : ~/Documents/Code/srv/yt/downloads/[SFM] Shrekophone
```

## MAKE IT A SERVICE

Je n'ai pas compris cette partie (Je voudrais bien des explications sur celle-ci)
