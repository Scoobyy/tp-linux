## Hostname

```sh=
gaby@toto:~$ sudo hostname node1.tp2.linux
# deco/reco
gaby@node1:~$ sudo nano /etc/hostname
gaby@node1:~$ cat /etc/hostname
node1.tp2.linux
```

# Partie 1 : SSH

## Installation 

```sh=
gaby@node1:~$ sudo apt-get install openssh-server
```


## Lancement

```sh=
gaby@node1:~$ systemctl start sshd
gaby@node1:~$ systemctl status sshd
```

## Etude

```sh=
gaby@node1:~$ systemctl status sshd

gaby@node1:~$ ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-10-25 13:31:52 CEST; 3h 6min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 537 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 4.1M
     CGroup: /system.slice/ssh.service
             └─537 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

oct. 25 13:31:52 gaby-Vm systemd[1]: Starting OpenBSD Secure Shell server...
oct. 25 13:31:52 gaby-Vm sshd[537]: Server listening on 0.0.0.0 port 22.
oct. 25 13:31:52 gaby-Vm sshd[537]: Server listening on :: port 22.
oct. 25 13:31:52 gaby-Vm systemd[1]: Started OpenBSD Secure Shell server.
oct. 25 16:03:09 toto sshd[24237]: Accepted password for gaby from 192.168.183.1 port 64445 ssh2
oct. 25 16:03:09 toto sshd[24237]: pam_unix(sshd:session): session opened for user gaby by (uid=0)
oct. 25 16:04:33 node1.tp2.linux sshd[24337]: Accepted password for gaby from 192.168.183.1 port 51228 ssh2
oct. 25 16:04:33 node1.tp2.linux sshd[24337]: pam_unix(sshd:session): session opened for user gaby by (uid=0)

gaby@node1:~$ ps -A | grep -F ssh
    537 ?        00:00:00 sshd
    918 ?        00:00:00 ssh-agent
  24337 ?        00:00:00 sshd
  24390 ?        00:00:00 sshd

gaby@node1:~$ ss -l | grep -F ssh
u_str   LISTEN   0        4096             /run/user/1000/gnupg/S.gpg-agent.ssh 26343                                           * 0
u_str   LISTEN   0        10                         /run/user/1000/keyring/ssh 26905                                           * 0
u_str   LISTEN   0        64               /var/lib/fwupd/gnupg/S.gpg-agent.ssh 134721                                          * 0
u_str   LISTEN   0        128                   /tmp/ssh-uivmsu4gM1nx/agent.835 26630                                           * 0
tcp     LISTEN   0        128                                           0.0.0.0:ssh                                       0.0.0.0:*
tcp     LISTEN   0        128                                              [::]:ssh                                          [::]:*

gaby@node1:~$ cd /var/log/

gaby@node1:/var/log$ journalctl | grep -F ssh
oct. 25 16:48:07 node1.tp2.linux sshd[24390]: Received disconnect from 192.168.183.1 port 51228:11: disconnected by user
oct. 25 16:48:07 node1.tp2.linux sshd[24390]: Disconnected from user gaby 192.168.183.1 port 51228
oct. 25 16:48:07 node1.tp2.linux sshd[24337]: pam_unix(sshd:session): session closed for user gaby
oct. 25 16:49:20 node1.tp2.linux sshd[24613]: Accepted password for gaby from 192.168.183.1 port 58495 ssh2
oct. 25 16:49:20 node1.tp2.linux sshd[24613]: pam_unix(sshd:session): session opened for user gaby by (uid=0)
C:\Users\Utilisateur>ssh gaby@192.168.183.11
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

51 updates can be applied immediately.
To see these additional updates run: apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Mon Oct 25 16:49:20 2021 from 192.168.183.1
```

## Modification de la configuration du serveur

```sh=
gaby@node1:~$ sudo nano /etc/ssh/sshd_config

gaby@node1:~$ cat /etc/ssh/sshd_config | grep Port
Port 1111
#GatewayPorts n

gaby@node1:~$ sudo systemctl restart sshd

gaby@node1:~$ systemctl status sshd
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-10-25 17:52:42 CEST; 4s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 25046 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 25047 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 1.0M
     CGroup: /system.slice/ssh.service
             └─25047 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 s

gaby@node1:~$ sudo ss -ltnp
State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
LISTEN   0        4096        127.0.0.53%lo:53              0.0.0.0:*       users:(("systemd-resolve",pid=400,fd=13))
LISTEN   0        128               0.0.0.0:1111            0.0.0.0:*       users:(("sshd",pid=531,fd=3))
LISTEN   0        5               127.0.0.1:631             0.0.0.0:*       users:(("cupsd",pid=434,fd=7))
LISTEN   0        128                  [::]:1111               [::]:*       users:(("sshd",pid=531,fd=4))
LISTEN   0        5                   [::1]:631                [::]:*       users:(("cupsd",pid=434,fd=6))

gaby@node1:~$ exit

C:\Users\Utilisateur>ssh gaby@192.168.183.11 -p 1111
gaby@192.168.183.11's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)
[...]
```

# Partie 2 : FTP

## Installation du serveur : 

```shell=
gaby@node1:~$ sudo apt-get install vsftpd
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following NEW packages will be installed:
  vsftpd
0 upgraded, 1 newly installed, 0 to remove and 62 not upgraded.
Need to get 115 kB of archives.
After this operation, 338 kB of additional disk space will be used.
Get:1 http://fr.archive.ubuntu.com/ubuntu focal/main amd64 vsftpd amd64 3.0.3-12 [115 kB]
Fetched 115 kB in 0s (728 kB/s)
Preconfiguring packages ...
Selecting previously unselected package vsftpd.
(Reading database ... 199592 files and directories currently installed.)
Preparing to unpack .../vsftpd_3.0.3-12_amd64.deb ...
Unpacking vsftpd (3.0.3-12) ...
Setting up vsftpd (3.0.3-12) ...
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for systemd (245.4-4ubuntu3.11) ...

gaby@node1:~$ cd /etc/

gaby@node1:/etc$ cat vsftpd.conf
# Example config file /etc/vsftpd.conf
#
# The default compiled in settings are fairly paranoid. This sample file
# loosens things up a bit, to make the ftp daemon more usable.
# Please see vsftpd.conf.5 for all compiled in defaults.
#
# READ THIS: This example file is NOT an exhaustive list of vsftpd options.
# Please read the vsftpd.conf.5 manual page to get a full idea of vsftpd's
# capabilities.
[...]
```

## Lancement du service FTP :

```shell=
gaby@node1:~$ systemctl start vsftpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to start 'vsftpd.service'.
Authenticating as: Gaby,,, (gaby)
Password:
==== AUTHENTICATION COMPLETE ===

gaby@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 20:21:42 CET; 8min ago
   Main PID: 1753 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 532.0K
     CGroup: /system.slice/vsftpd.service
             └─1753 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 08 20:21:42 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 20:21:42 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```

## Etude du service FTP 

```shell=
gaby@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 20:21:42 CET; 52min ago
   Main PID: 1753 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 532.0K
     CGroup: /system.slice/vsftpd.service
             └─1753 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 08 20:21:42 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 20:21:42 node1.tp2.linux systemd[1]: Started vsftpd FTP server.

gaby@node1:~$ ps -A | grep vsftpd
   1753 ?        00:00:00 vsftpd
   
gaby@node1:~$ ss -ltnp

State          Recv-Q         Send-Q                 Local Address:Port                   Peer Address:Port         Process
[...]
LISTEN         0              32                                 *:21                                *:*
[...]

gaby@node1:~$ journalctl | grep -F vsftpd
nov. 08 20:21:40 node1.tp2.linux sudo[1581]:     gaby : TTY=pts/1 ; PWD=/home/gaby ; USER=root ; COMMAND=/usr/bin/apt-get install vsftpd
nov. 08 20:21:42 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 20:21:42 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
[...]
```

# Partie 3 : Création de votre propre service

## Jouer avec netcat``

```shell=
gaby@node1:~$ sudo apt-get install netcat
```
 
### Sur la VM :
 
```shell=
gaby@node1:~$ nc -l 1054
```

### Sur une autre VM : 
```shell=
gaby@gaby-Vm:~$ nc 192.168.183.11 1054

gaby@gaby-Vm:~$ nc 192.168.183.11 1054 > discu
```

## Un Service Basé sur netcat

```shell=
gaby@node1:~$ nano /etc/systemd/system/chat_tp2.service

gaby@node1:~$ sudo chmod 777 /etc/systemd/system/chat_tp2.service

gaby@node1:~$ which nc
usr/bin/nc

gaby@node1:~$ nc -l 192.168.183.11 1054 > etc/systemd/system/chat_tp2.service

gaby@node1:~$ nc 192.168.183.11 1054
[Unit]
Description=Little chat service (TP2)
[Service]
ExecStart=/usr/bin/nc
[Install]
WantedBy=multi-user.target

gaby@node1:~$ sudo systemctl daemon-reload

gaby@node1:~$ sudo systemctl start chat_tp2

gaby@node1:~$ systemctl status chat_tp2

gaby@node1:~$ ss -l 
[...]
tcp listen 0 1 0,0,0,0:1054
[...]

gaby@node1:~$ journalctl -xe -u chat_tp2 -f
shrek is love
shrek is life
```

