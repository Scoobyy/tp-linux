#!/bin/bash

if [[ -d /srv/yt/downloads ]]
then
if [[ -d /var/log/yt ]]
then
cd downloads
url=$1
nom=$(youtube-dl -e --skip-download $url)
mkdir "$titre"
cd "$titre"
mkdir description
cd description
youtube-dl --get-description $url >> la_description
cd -
youtube-dl $url >> /dev/nul
echo video $url was downloaded
echo File path : $(pwd) / $titre .mp4
echo $(date "+%y")/$(date "+%m")/$(date "+%d") $(date "+%T") Video $url was downloaded. File path : $(pwd) >> /var/log/yt/download.log
fi
fi
exit