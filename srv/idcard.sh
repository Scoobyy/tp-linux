#!/bin/bash

echo Machine name: $(hostname)

Os=$(hostnamectl | grep Operating | cut -d ' ' -f3-)
echo OS: $Os and the kernel version is $(uname -r)

IP=$(hostname -i | awk '{ print $1 }')
echo IP: $IP

RamTotal=$(free -t -h | grep Mem: | awk '{ print $2}')
RamRes=$(free -t -h | grep Mem: | awk '{ print $7}')
echo RAM: $RamRes RAM restante sur $RamTotal RAM totale

Stockage=$(df -h / | grep /dev | awk '{ print $4 }')
echo Disque : $Stockage space left


TOPFIVE=$(ps aux --sort=-%mem | head -5 | tail -4 | awk '{ print $11 }') \n$(ps aux --sort=-%mem | head -6 | tail -1 | awk '{ print $12 }')\n)
echo Top 5 processes by RAM usage :
echo $TOPFIVE

PORT=$(sudo lsof -i -P -n | grep LISTEN | awk '{ print $2 " : " $1 }')\n)
echo Listening ports :
echo $PORT


cat=$(curl -s https://api.thecatapi.com/v1/images/search | cut -d ':' -f4-5 | cut -d ',' -f1 | cut -d '"' -f2)
echo "Here's your random cat :" $cat 
