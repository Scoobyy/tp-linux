# TP4 : Une distribution orientée serveur

## II : Checklist : 

**Dans le fichier /etc/sysconfig/network-scripts/ifcfg-enp0s8**

```bash=
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.250.1.17
NETMASK=255.255.255.0
```
**Dans le Terminal de la VM :** 

```bash=
[scooby@localhost ~]$ ip a 

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7b:e2:99 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85064sec preferred_lft 85064sec
    inet6 fe80::a00:27ff:fe7b:e299/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:da:ce:6b brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.17/24 brd 10.250.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feda:ce6b/64 scope link 
       valid_lft forever preferred_lft forever
```

## Connexion SSH fonctionnelle

```bash=
[scooby@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-11-24 14:12:03 CET; 31min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 831 (sshd)
    Tasks: 1 (limit: 4935)
   Memory: 3.9M
   CGroup: /system.slice/sshd.service
           └─831 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256>

Nov 24 14:12:03 localhost.localdomain systemd[1]: Starting OpenSSH server daemon...
Nov 24 14:12:03 localhost.localdomain sshd[831]: Server listening on 0.0.0.0 port 22.
Nov 24 14:12:03 localhost.localdomain sshd[831]: Server listening on :: port 22.
Nov 24 14:12:03 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
Nov 24 14:13:39 localhost.localdomain sshd[1429]: Accepted password for scooby from 10.250.1.1 port 34030 ssh2
Nov 24 14:13:39 localhost.localdomain sshd[1429]: pam_unix(sshd:session): session opened for user scooby by (uid=0)
```

## Connexion ssh avec échange de clés : 

```bash=
~ > cat .ssh/id_ed25519.pub                                                      
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOp8saIl7yWg8BOFYBZ6T1VF8jmPwtRKGSRQ4IFdDeOJ gaby@laroumanie-aspirea51556

[scooby@localhost ~]$ cat .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCjflySjPgfyewtB/EvO4u3WsrTGni7XkLUPIdmOnMi9osPS1qBq0XKrtlUWKDsxNUnQOsJfs14jb61aSuXUknMdwWS7pixcD4CF8HIFeqKeNmj/Qd2/iuwFKx3/1XhOJZ/BHxebqlo7h7bmBHc5/41x9tkVGg6hPCMurMwFzX/HB9YfGZG6BlYPkgWF7EX8M5tKegK5SYR/Lx+pdi934F+7WabVnMR1BfF11iYh8gECHe9+IA7CKgdTz4M1wTQBrOkxT+5YfHnMFCi3NtgEntwC2zRPiP5P/r6qqaaN/FUoeBVUcIaAnT7Km+Z4iexHrBwqGICn3EoiQvEOgyLIhZj/WVxL5a9hv7bQR6q40DRQGzTTUhV7tKTBfI40qrhZP0oa3IssHWEIugmzv7MS/JfGl60NmIf6ArggSE6n+XFDvpIg8nAUjjH83tVHM8xt1W22E3J491Ge3I4FBUFZMiAXdlcgoknrImUrcCwqZu9GnRUc7Y/xo/3y/R+2sVWY2DMK5nTiniIKfr2GIvfy0dlC3FaYhU5gu1ZElJnzwPKT4uVYEcBz35AQpFHyuJOiOMBw1p2FyWJXlBv5Sj0T6TJ+buWaY6A3rLzH0p3Dt9y4dGWGe3HwiaK2xsx3qQfPS//w0a4+7KoVLxfIBc0oJyzn/uUDfW2ft2oUmYwpA7Okw== gaby@laroumanie-aspirea51556

~ > ssh scooby@10.250.1.17
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Nov 24 14:50:56 2021 from 10.250.1.1
[scooby@localhost ~]$ 
```

## Accès Internet : 

``` bash=
[scooby@localhost ~]$ ping -c 4 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=25.8 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=22.10 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=23.8 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=63 time=23.6 ms

--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 22.952/24.023/25.824/1.097 ms


[scooby@localhost ~]$ ping -c 4 youtube.com
PING youtube.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=63 time=23.7 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=63 time=23.10 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=63 time=26.3 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=4 ttl=63 time=24.0 ms

--- youtube.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 23.650/24.479/26.251/1.050 ms
```

## Nommage de la machine : 

```bash=
[scooby@localhost ~]$ sudo hostname node1.tp4.linux

[scooby@localhost ~]$ sudo nano /etc/hostname

[scooby@localhost ~]$ cat /etc/hostname
node1.tp4.linux

[scooby@localhost ~]$ exit

~ > ssh scooby@10.250.1.17
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Nov 24 16:54:33 2021 from 10.250.1.1

[scooby@node1 ~]$ hostname
node1.tp4.linux
```

# Intro Nginx

## Installation NGINX

```bash=
[scooby@node1 ~]$ sudo dnf install nginx
Last metadata expiration check: 2:08:27 ago on Wed 24 Nov 2021 02:56:48 PM CET.
Dependencies resolved
[...]
  perl-threads-1:2.21-2.el8.x86_64                                                                                  
  perl-threads-shared-1.58-2.el8.x86_64                                                                             

Complete!
```

## Analyse 

```bash=
[scooby@node1 ~]$ sudo systemctl start nginx

[scooby@node1 ~]$ sudo systemctl status nginx.service 
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-24 17:14:07 CET; 16s ago
  Process: 26571 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 26570 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 26568 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 26573 (nginx)
    Tasks: 2 (limit: 4935)
   Memory: 3.7M
   CGroup: /system.slice/nginx.service
           ├─26573 nginx: master process /usr/sbin/nginx
           └─26574 nginx: worker process

Nov 24 17:14:06 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 24 17:14:07 node1.tp4.linux nginx[26570]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 24 17:14:07 node1.tp4.linux nginx[26570]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 24 17:14:07 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid ar>
Nov 24 17:14:07 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```

## Analyse du service NGINX

```bash=
[scooby@node1 ~]$ ps -aux | grep nginx
root       26573  0.0  0.2 119152  2168 ?        Ss   17:14   0:00 nginx: master process /usr/sbin/nginx
nginx      26574  0.0  0.9 151804  7948 ?        S    17:14   0:00 nginx: worker process
scooby     26648  0.0  0.1 221928  1120 pts/0    R+   17:19   0:00 grep --color=auto nginx


[scooby@node1 ~]$ sudo ss -lntp | grep nginx
[sudo] password for scooby: 
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=26574,fd=8),("nginx",pid=26573,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=26574,fd=9),("nginx",pid=26573,fd=9))

[scooby@node1 usr]$ cd /etc/usr/share/nginx/html/
[scooby@node1 html]$ ls
404.html  50x.html  index.html  nginx-logo.png  poweredby.png

[scooby@node1 html]$ ls -al
total 20
drwxr-xr-x. 2 root root   99 Nov 24 17:05 .
drwxr-xr-x. 4 root root   33 Nov 24 17:05 ..
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png

```

## Configuration du firewall : 

```bash=
[scooby@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success

[scooby@node1 ~]$ sudo firewall-cmd --reload
success
```

## Test Fonctionnement : 

```bash=
curl http://10.250.1.17:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
```

# Modification de la conf du serveur : 

## Changer le port d'écoute : 

```bash=

[scooby@node1 nginx]$ cat nginx.conf
[...]
server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
[...]

[scooby@node1 nginx]$ sudo systemctl restart nginx.service 

[scooby@node1 nginx]$ systemctl status nginx.service 
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-24 18:02:43 CET; 41s ago
  Process: 26848 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 26846 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 26844 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 26850 (nginx)
    Tasks: 2 (limit: 4935)
   Memory: 3.6M
   CGroup: /system.slice/nginx.service
           ├─26850 nginx: master process /usr/sbin/nginx
           └─26851 nginx: worker process

Nov 24 18:02:43 node1.tp4.linux systemd[1]: nginx.service: Succeeded.
Nov 24 18:02:43 node1.tp4.linux systemd[1]: Stopped The nginx HTTP and reverse proxy server.
Nov 24 18:02:43 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 24 18:02:43 node1.tp4.linux nginx[26846]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 24 18:02:43 node1.tp4.linux nginx[26846]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 24 18:02:43 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid ar>
Nov 24 18:02:43 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.


[scooby@node1 nginx]$ sudo ss -lntp | grep nginx
LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=26851,fd=8),("nginx",pid=26850,fd=8))
LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=26851,fd=9),("nginx",pid=26850,fd=9))

[scooby@node1 nginx]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success

[scooby@node1 nginx]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success

[scooby@node1 nginx]$ sudo firewall-cmd --reload 
success

[scooby@node1 nginx]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 8080/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

[scooby@node1 nginx]$ curl http://10.250.1.17:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>

```

## Changer l'utilisateur qui lance le service : 

```bash=

[scooby@node1 nginx]$ sudo useradd -m -d /home/web web -p toto

[scooby@node1 ~]$ tail -n 1 /etc/passwd | grep web
web:x:1001:1001::/home/web:/bin/bash

[scooby@node1 ~]$ head -n 5 /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user web;

[scooby@node1 ~]$ ps -aux | grep nginx
root       27490  0.0  0.2 119152  2168 ?        Ss   19:00   0:00 nginx: master process /usr/sbin/nginx
web        27491  0.0  0.9 151804  7916 ?        S    19:00   0:00 nginx: worker process
scooby     27521  0.0  0.1 221928  1064 pts/0    R+   19:02   0:00 grep --color=auto nginx
```

## Changer l'emplacement de la racine web : 

```bash= 

[scooby@node1 var]$ sudo mkdir www

[scooby@node1 var]$ sudo mkdir www/super_site_seb

[scooby@node1 var]$ cd www/super_site_seb/

[scooby@node1 super_site_seb]$ sudo nano index.html

[scooby@node1 super_site_seb]$ sudo chown web /var/www

[scooby@node1 super_site_seb]$ sudo chown web /var/www/super_site_seb/

[scooby@node1 super_site_seb]$ sudo chown web /var/www/super_site_seb/index.html

[scooby@node1 super_site_seb]$ sudo nano /etc/nginx/nginx.conf

[scooby@node1 super_site_seb]$ cat /etc/nginx/nginx.conf
[...]
    server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /var/www/super_site_web/index.html;
[...]

[scooby@node1 ~]$ sudo systemctl restart nginx.service 

[scooby@node1 www]$ curl http://10.250.1.17:8080
<h1>J'aime Shrek</h1>

<p> Je peux avoir avoir une bonne note stp :)  </p>

```
